(function() { 
    'use strict';

    var adminApp = angular.module('adminApp.cardsController', []);

    adminApp.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('cards', {
                url: '/',
                templateUrl : 'components/cards/cardsView.html',
                controller: 'CardsController'
            });    
    }]);

    adminApp.controller('CardsController', ['$scope', '$http', '$mdDialog', function($scope, $http, $mdDialog) {

        $http({
            method  : 'GET',
            url     : '/blog-pages'
        })
        .then(function successCallback(data){
            $scope.listPage = data;        

        }, function errorCallback(response){
            console.log(response.status);
        }); 

        $scope.wordsLimit = {
            content:100,
            headline: 20
        };        

        $scope.customFullscreen = false;

        $scope.icons = [{
            icon: 'favorite',
            label: 'Favorite',
            func: 'likePhoto',
            cssClass: 'red-heart'
        }, {
            icon: 'visibility',
            label: 'Visibility',
            func: 'readBlog',
            cssClass: ''            
        }];

        $scope.buttonAction = function(func, item){
            if (func === 'readBlog') {
                this.showContent(item);
            }
            if (func === 'likePhoto') {
                console.log('I like it!');
            }
        }

        $scope.showContent = function(item) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/directives/md-dialog-blog.html',
                parent: angular.element(document.body),
                // targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                item: item
            });
        };  

        function DialogController($scope, $mdDialog, item) {
            $scope.blogContent = item;
            $scope.icon = {
                name: 'close',
                label: 'Close',                
            }
            $scope.closeDialog = function() {
                $mdDialog.hide();
            }
        }                      

    }]);

})();