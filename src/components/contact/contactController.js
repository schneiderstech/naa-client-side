(function() { 
    'use strict';

    var adminApp = angular.module('adminApp.contactController', []);

    adminApp.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('contact', {
                url: '/',
                templateUrl : 'components/contact/contactView.html',
                controller: 'ContactController'
            });    
    }]);

    adminApp.controller('ContactController', function($scope, $http) {

        $http({
            method  : 'GET',
            url     : '/contact'
        })
        .then(function successCallback(data){
            $scope.listPage = data; 

            
        }, function errorCallback(response){
            console.log(response.status);
        });        
        $scope.icons = {
            path: '/assets/img/icons/', 
            icon: {
                person: 'ic_person_24px.svg',
                email: 'ic_email_24px.svg',
                phone: 'ic_call_24px.svg',
                place: 'ic_place_24px.svg'
            }
        } 

        $scope.submitContact = function(valid) {
            if(valid) {
                console.log('Form is valid?', valid);
                return;
            }
            console.log('Form is valid?', valid);
        }   
    });

})();