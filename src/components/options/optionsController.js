(function() { 
    'use strict';

    var adminApp = angular.module('adminApp.optionsController', []);

    adminApp.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('options', {
                url: '/options',
                templateUrl : 'optionsView.html',
                controller: 'OptionsController'
            });    
    }]);

    adminApp.controller('OptionsController', function($scope, $mdDialog) {
        var originatorEv;

        var standardPath = '/assets/img/icons/';
        
        $scope.pageSettings = {
            menuContentWidth: 4,
            profile: {
                path: standardPath,
                name: 'ic_person_24px.svg',
                alt: 'Profile', 
                title: 'Profile'
            },
            notification: {
                path: standardPath,
                on: {
                    name: 'ic_notifications_24px.svg',
                    status: 'Enabled'
                },
                off: {
                    name: 'ic_notifications_off_24px.svg',
                    status: 'Disabled'  
                },
                alt: 'Notification',
                title: 'notification'
            }    
        }

        this.openOptions = function($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };

        this.notificationsEnabled = true;
        this.toggleNotifications = function() {
            this.notificationsEnabled = !this.notificationsEnabled;
        };
    });

})();