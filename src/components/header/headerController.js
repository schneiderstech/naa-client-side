(function() { 
    'use strict';

    var adminApp = angular.module('adminApp.headerController', []);

    adminApp.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('header', {
                url: '/',
                templateUrl : 'headerView.html',
                controller: 'headerController'
            });    
    }]);

    adminApp.controller('HeaderController', function($scope) {
        var standardPath = '/assets/img/icons/';
        
        $scope.pageSettings = {
            title: 'AngularJS + MD',
            home: {
                path: standardPath,
                name: 'ic_home_24px.svg',
                alt: 'Home'
            },
            logo: {
                path: standardPath,
                name: 'angular-logo.svg',
                alt: 'Angular logo'
            }    
        }
    });
})();