(function() { 
    'use strict';

    var adminApp = angular.module('adminApp.blogController', []);

    adminApp.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('blog', {
                url: '/',
                templateUrl : 'components/blog/blogView.html',
                controller: 'BlogController'
            });    
    }]);

    adminApp.controller('BlogController', function($scope, $http) {

        $http({
            method  : 'GET',
            url     : '/blog'
        })
        .then(function successCallback(data){
            $scope.listPage = data; 
            //  trustAsHtml 
            // $scope.$sce = $sce;
            
        }, function errorCallback(response){
            console.log(response.status);
        });   

        $scope.images = {
            img1: 'CSC_1570',
            img2: 'DSC_1500',
            img3: 'DSC_1520',
            img4: 'DSC_1524',
            img5: 'DSC_1540',
            img6: 'DSC_1537'
        }
             
    
    });

})();