(function() { 
    'use strict';

    var adminApp = angular.module('adminApp.aboutController', []);

    adminApp.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('about', {
                url: '/',
                templateUrl : 'components/about/aboutView.html',
                controller: 'AboutController'
            });    
    }]);

    adminApp.controller('AboutController', function($scope, $http) {

        $http({
            method  : 'GET',
            url     : '/about'
        })
        .then(function successCallback(data){
            $scope.listPage = data;             

        }, function errorCallback(response){
            console.log(response.status);
        }); 

        $scope.likes = 100;  

        var count = 0;
        var myLike = false;

        $scope.countLikes = function() {
            count = $scope.likes + 1;
            $scope.likes = count;    
            myLike = true;

            if(myLike) {
                $scope.heart = 'red-heart'
            }            
        }


        $scope.icons = {
            path: '/assets/img/icons/', // not in use, since it's css
            icon: {
                favorite: 'favorite'
            }
        }
             
    
    });

})();