(function() { 
    'use strict';

    var adminApp = angular.module('adminApp.menuController', []);

    adminApp.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('menu', {
                url: '/',
                templateUrl : 'menuView.html',
                controller: 'MenuController'
            });    
    }]);

    adminApp.controller('MenuController', function($scope, $location, $state) {

        var originatorEv;
        var standardPath = '/assets/img/icons/';

        this.openMenu = function($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };
    
        $scope.pageSettings = {
            title: 'Menu',
            menuContentWidth: 4,
            menuItems: [
                {
                    title: 'About',
                    buttonAction: {
                        key:'aboutPage',
                        value: 1,
                        link: 'about'
                    },
                    icon: {
                        path: standardPath,
                        name: 'ic_assignment_ind_24px.svg',
                    }
                },
                {
                    title: 'Contact',
                    buttonAction: {
                        key: 'contactPage',
                        value: 2,
                        link: 'contact'
                    },
                    icon: {
                        path: standardPath,
                        name: 'ic_email_24px.svg'
                    }
                },
                {
                    title: 'Blog',
                    buttonAction: {
                        key: 'blogPage',
                        value: 3,
                        link: 'blog'
                    },
                    icon: {
                        path: standardPath,
                        name: 'ic_question_answer_24px.svg'
                    }
                }
            ]
        };

        $scope.buttonAction = function(buttonAction) {
            var linkRouteProvider = buttonAction.link;
            $state.go(linkRouteProvider);    
        };

    });
})();