(function() { 
    // Declare app level module which depends on views, and components
    var adminApp = angular.module('adminApp', [
        'ui.router',
        'ngSanitize',
        'ngAnimate',
        'ngMaterial',
        'ngCookies',
        'adminApp.app',
        'adminApp.headerController',
        'adminApp.menuController',
        'adminApp.optionsController',
        'adminApp.cardsController',
        'adminApp.aboutController',
        'adminApp.contactController',
        'adminApp.blogController'        
    ]);

    adminApp.config(function($locationProvider, $urlRouterProvider){    
        $urlRouterProvider.otherwise('/');
        // to remove #! from url
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });    
    });
})();
