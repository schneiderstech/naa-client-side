(function() { 
    'use strict';
    var adminApp = angular.module('adminApp.app', []);

    adminApp.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl : 'views/mainView.html',
                controller: 'AppController'
            });    
    }]);

    adminApp.controller('AppController', ['$scope', '$location', function($scope, $location) {

        $scope.pageSettings = {
            scroolTop: {
                img: 'ic_arrow_up_24px',
                alt: 'Back to top'
            }
        }

        $scope.scrollToTop = function() {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('.back-to-top').fadeIn();
                } else {
                    $('.back-to-top').fadeOut();
                }
            });

            $('.back-to-top').click(function () {
                $("html, body").animate({ scrollTop: 0 }, 500);
                return false;
            });
        };

    }]);

    adminApp.directive('directiveBackToTop', function() {
        return {
            templateUrl: '/directives/back-to-top.html'
        };
    });

})();